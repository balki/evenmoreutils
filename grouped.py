from enum import Enum, auto


class ReminderError(ValueError):
    pass


class ReminderPolicy(Enum):
    IGNORE = auto()
    RETURN = auto()
    FILL = auto()
    ERROR = auto()


def grouped_ignore_or_error(itr, n, error):
    itr = iter(itr)
    end = object()
    while True:
        vals = tuple(next(itr, end) for _ in range(n))
        if vals[-1] is end:
            if error and vals[0] != end:
                raise ReminderError("Found reminder")
            return
        yield vals


def grouped_return(itr, n):
    itr = iter(itr)
    while True:
        vals = tuple(v for v, _ in zip(itr, range(n)))
        if not vals:
            return
        yield vals
        

def grouped(itr, n=2, policy = ReminderPolicy.IGNORE, fill_value = None):
    if n < 1:
        raise ValueError("Invalid n")
    if policy == ReminderPolicy.IGNORE:
        yield from grouped_ignore(itr, n, False)
    elif policy == ReminderPolicy.ERROR:
        yield from grouped_ignore(itr, n, True)
    elif policy == ReminderError.RETURN:
        yield from grouped_return(itr, n)
    else:
        for vals in grouped_return(itr, n):
            if len(vals) != n:
                return vals + tuple(fill_value for _ in range(n - len(vals)))
            yield vals
