#!/usr/bin/python3

from pathlib import Path
from email import message_from_binary_file

import sys


def get_text(mail_path: Path) -> str:

    with mail_path.open('rb') as fp:

        msg = message_from_binary_file(fp)

    for part in msg.walk():

        if part.get_content_type() == 'text/plain':

            mail_data = part.get_payload(decode=True)

            return mail_data.decode()

    return "Not found"


if __name__ == '__main__':

    print(get_text(Path(sys.argv[1])))
