#!/usr/bin/python3

from urllib.parse import urlencode

import sys


def gen_iv_url(url, rhash):
    query = urlencode({"url": url, "rhash": rhash})
    return f'https://t.me/iv?{query}'


def main():
    rhash = sys.argv[1]
    for line in sys.stdin:
        if line.startswith('https://'):
            url = line.strip()
            iv_url = gen_iv_url(url, rhash)
            text = f'<a href="{iv_url}">➢</a> <a href="{url}">Link</a>'
            print(text, flush=True)

if __name__ == '__main__':
    main()
