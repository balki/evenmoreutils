#!/usr/bin/python3

import sys


def main():

    cache_file = sys.argv[1]
    from_stdin = sys.stdin.read()

    if not from_stdin:
        return

    try:
        from_cache = open(cache_file).read()

    except FileNotFoundError:
        from_cache = ''


    if from_stdin != from_cache:
        open(cache_file, 'w').write(from_stdin)
        print(from_stdin, end='')


if __name__ == '__main__':
    main()
