#!/usr/bin/python3

import random, sys


def main():

    n = int(sys.argv[1])

    if random.randint(1, n) == 1:
        exit(0) # Means success

    exit(1) # Means failure

if __name__ == '__main__':
    main()
