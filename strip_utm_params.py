#!/usr/bin/python3

from urllib.parse import urlparse, ParseResult, parse_qs, urlencode, urlunparse

import sys


def strip_utm_stuff(url):
    parse_result: ParseResult = urlparse(url)
    query_d = parse_qs(parse_result.query)
    query_d = {k: v for k, v in query_d.items() if not k.startswith("utm_")}
    parse_result = parse_result._replace(query=urlencode(query_d, doseq=True))
    return urlunparse(parse_result)


def main():
    for line in sys.stdin:
        if line.startswith('https://'):
            url = line.strip()
            url = strip_utm_stuff(url)
            print(url, flush=True)

if __name__ == '__main__':
    main()
