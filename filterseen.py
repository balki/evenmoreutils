#!/usr/bin/python3

import dbm, sys
from datetime import date


def filterseen(db, stream, val):
    for line in stream:
        line = line.strip()
        if line not in db:
            yield line
        db[line] = val


def main():
    db = dbm.open(sys.argv[1], 'c') # 'c' -> read/write access to new or existing db
    today = str(date.today())
    try:
        for line in filterseen(db, sys.stdin, today):
            # Flushing to minimize buffering
            print(line, flush=True)
    finally:
        db.reorganize()
        db.close()


if __name__ == '__main__':
    main()
